# PyDeTide 


### Description
Tool for removing the astronomical tide from sea level signals.
The tool is based on the `rlowess` and `rloess` filtering methods of the MATLAB function smooth (http://it.mathworks.com/help/curvefit/smooth.html) and it is composed by two files:

* `smooth_data.py`: the module containing the filtering functions
* `pydetide.py`   : a simple graphical user interface (GUI) to plot both the original and the filtered signals


### Pre-requisites:
* **numpy** (to use the `smooth_data.py` module)
* **wx** and **matplotlib** (only to run the `pydetide.py` GUI and visualize your data analysis, but not necessary to run the `smooth_data.py` module)


### Installation and run

#### As standalone tool
Download the tool, open a terminal and run:
```
$ unzip pydetide-xxx.zip
$ cd pydetide-xxx/
$ python pydetide.py
```
and the main frame window should appear.

Then in the file menu, you can select open file and choose a time series to analyse.
Time series have to be formatted as two-columns text files (time and elevation). 

#### As module
Alternativele, one can locally save the `smooth_data.py` module in the PYTHON PATH and use it as any other Python module:
```
import smooth_data as sd
...
s = sd.smooth(x, y, span, method)

```
where (x, y) is the time series, **span** is the number of points after and before each point of the curve used for the smoothing and **method** is one among `rlowess` and `rloess`.
 

#### Running examples
In the folder `examples/` one can find and run an example to call and use the module `smooth_data.py`, using the following command:

```
python filter_sea_level.py iquique_2cols_24h.dat
```
If matplotlib is installed, the example script opens an interactive plot comparing the original sea level signal with the filtered one.
