#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
"""
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
sys.path.append("../")

import smooth_data as sm

mpl.rcParams['xtick.direction']='out'
mpl.rcParams['ytick.direction']='out'
mpl.rcParams['axes.labelsize']='12'
mpl.rcParams['xtick.labelsize']='12'
mpl.rcParams['ytick.labelsize']='12'
mpl.rcParams['legend.fontsize']='12'
mpl.rcParams['font.family']='serif'
mpl.rcParams['font.sans-serif']='Times'

nargv = len(sys.argv)

if (nargv <= 1):
    txt = """
    Usage:
    python filterTS.py filename span[optional] method[optional]
       
    - filename: two-columns ascii file with time and sea level
    - span:     number of points used to calculate filtered sea 
              level [default value 15] 
    - method:   name of the filtering method [default value "rloess"]
  
    The script import the smooth.py module.
  
    To be imported correctly, smooth.py must be in the python path or 
    in the same folder of filterTS.py
  
    """
    sys.exit(txt)
  
elif (nargv > 1):
    filename = sys.argv[1]
    if (nargv == 2):
        span = 250
        method = "rloess"
    elif (nargv == 3):
        span = float(sys.argv[2])
        method = "rloess"  
    elif (nargv == 4):  
        span = sys.argv[2]
        method = sys.argv[3]
    else:
        sys.exit("too many arguments")
    

data = np.loadtxt(filename)
t = data[:,0]
ts = data[:,1]
s = sm.smooth(t, ts, span, method)
res = ts-s

# plotting
plt.figure(figsize=(10,8))
plt.subplot(2,1,1)
plt.plot(t, ts, color="#000000", linewidth=1, label="Observed")
plt.plot(t, s, color="#ff0000", linewidth=1, label="Filtered")
plt.grid(True)
plt.ylabel("Sea level (m)")
plt.legend()

plt.subplot(2,1,2)
plt.plot(t, res, color="#0000ff", linewidth=1)
plt.axhline(y=0, color="#666666")
plt.ylim(min(res), max(res))
plt.grid(True)
plt.xlabel("Time (min)")
plt.ylabel("Residual (m)")

plt.show()
